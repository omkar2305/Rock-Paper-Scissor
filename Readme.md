# Rock-Paper-Scissor UI game

The objective of the rock, paper, scissor python project is to build a game for single player that plays with the computer.
This project is built on the rules:

-  Rock blunts Scissor so rock wins.
-  Scissor cuts the paper so Scissor win. 
-  Paper cover rock so paper win.

